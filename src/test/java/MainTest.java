import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import page.StartPage;
import util.Settings;
import util.Utils;

import static org.junit.Assert.assertTrue;

public class MainTest extends Settings {
    private static StartPage startPage;


    @BeforeClass
    public static void setUp() throws Exception {
        uploadApp();
        startPage = new StartPage(driver);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void test_success_recipe_title() { //Good -> Success
        System.out.println(startPage.getRecipeTitle());
        assertTrue(!startPage.getRecipeTitle().equals(""));
    }

    @Test
    public void test_success_find_letscook_button_on_main_page() {
        assertTrue(startPage.findLetsCookBtn());
    }

    @Test
    public void test_success_swipes() {
        startPage.clickLetsCookBtnOnMainPage();
        Utils.swipeLeft(driver);
        Utils.swipeDown(driver);
        Utils.swipeUp(driver);
        Utils.swipeRight(driver);
    }
}
package page;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class ErrorPage {
    private WebDriver driver;

    /**
     * Locators
     */
    private By errorTitle = By.xpath("");

    /**
     * Methods
     */
    public ErrorPage(IOSDriver driver) {
        this.driver = driver;
    }

    public void getErrorTitle() {
        driver.findElement(errorTitle).getText();
    }

        public boolean findErrorTitle() {
            try {
                driver.findElement(errorTitle);
            } catch (NoSuchElementException e) {
                return false;
        }
        return true;
    }
}

package page;

        import io.appium.java_client.ios.IOSDriver;
        import org.openqa.selenium.By;
        import org.openqa.selenium.NoSuchElementException;
        import org.openqa.selenium.WebDriver;

public class StartPage {
    private WebDriver driver;

    /**
     * Locators
     */
    private By letsCookBtn = By.id("Давай готовить уже!");
    private By recipeTitle = By.xpath("/XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeTextView");

    /**
     * Methods
     */
    public StartPage(IOSDriver driver) {
        this.driver = driver;
    }

    private void clickLetsCookBtn() {
        driver.findElement(letsCookBtn).click();
    }

    public void clickLetsCookBtnOnMainPage() {
        this.clickLetsCookBtn();
    }

    public boolean findLetsCookBtn() {
        try {
            driver.findElement(letsCookBtn);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public String getRecipeTitle() {
        return driver.findElement(recipeTitle).getText();
    }
}

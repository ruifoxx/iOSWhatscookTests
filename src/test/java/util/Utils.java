package util;

import io.appium.java_client.ios.IOSDriver;

public class Utils {

    public static void swipeLeft(IOSDriver driver) {
        driver.swipe(Settings.getWidth() - 5, Settings.getHeight() / 2, 1, Settings.getHeight() / 2, 0);
    }

    public static void swipeRight(IOSDriver driver) {
        driver.swipe(15, Settings.getHeight() / 2, Settings.getWidth() - 1, Settings.getHeight() / 2, 0);
    }

    public static void swipeDown(IOSDriver driver) {
        driver.swipe(Settings.getWidth() / 2, Settings.getHeight() - 25, Settings.getWidth() / 2, Settings.getHeight() / 2, 0);
    }

    public static void swipeUp(IOSDriver driver) {
        driver.swipe(Settings.getWidth() / 2, Settings.getHeight() / 2, Settings.getWidth() / 2, Settings.getHeight() - 25, 0);
    }
}

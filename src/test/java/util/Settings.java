package util;

import io.appium.java_client.ios.IOSDriver;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Settings {
    public static IOSDriver driver;
    public static int height = 0;
    public static int width = 0;
    @Rule
    public TestRule printTests = new TestWatcher() {
        protected void starting(Description description) {
            System.out.println("\n" + "( ͡° ͜ʖ ͡°) Start Test: " + description.getMethodName());
        }

        protected void finished(Description description) {
            System.out.println("( ͡° ͜ʖ ͡°) Finish Test: " + description.getMethodName());
        }
    };

    /**
     * Upload app to device
     */
    protected static void uploadApp() throws Exception {
        String appName = "whatscook.app";
        String appiumURL = "http://127.0.0.1:4723/wd/hub";

        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, "");
        File application = new File(appDir, appName);

        //TODO: задавать разные capabilities для тестирования на разных усройствах (параметры?)
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("platformVersion", "10.3");
        capabilities.setCapability("deviceName", "iPhone 7");
        capabilities.setCapability("app", application.getAbsolutePath());

        driver = new IOSDriver(new URL(appiumURL), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        height = driver.manage().window().getSize().height;
        width = driver.manage().window().getSize().width;
    }

    public static int getHeight() {
        return height;
    }

    public static int getWidth() {
        return width;
    }
}

import org.junit.Test;
import page.ErrorPage;

import static org.junit.Assert.assertTrue;
import static util.Settings.driver;

/**
 * Created by ruifo on 07.08.2017.
 */
public class ErrorTest {
    private static ErrorPage errorPage;

    public ErrorTest() {
        errorPage = new ErrorPage(driver);
    }

    @Test
    public void test_success_error_no_internet_connection_for_the_first_app_launch() {
        errorPage.getErrorTitle();
        assertTrue(errorPage.findErrorTitle());
    }
}
